package entity

import (
	"time"
)

type Message struct {
	Topic      string
	Data       interface{}
	LastUpdate time.Time
}
