package entity

// OrderInfo define order info
type OrderInfo struct {
	Symbol           string `json:"symbol"`
	OrderID          int64  `json:"orderId"`
	ClientOrderID    string `json:"clientOrderId"`
	Price            string `json:"price"`
	ReduceOnly       bool   `json:"reduceOnly"`
	OrigQuantity     string `json:"origQty"`
	ExecutedQuantity string `json:"executedQty"`
	CumQuantity      string `json:"cumQty"`
	CumQuote         string `json:"cumQuote"`
	Status           string `json:"status"`
	TimeInForce      string `json:"timeInForce"`
	Type             string `json:"type"`
	Side             string `json:"side"`
	StopPrice        string `json:"stopPrice"`
	Time             int64  `json:"time"`
	UpdateTime       int64  `json:"updateTime"`
	WorkingType      string `json:"workingType"`
	ActivatePrice    string `json:"activatePrice"`
	PriceRate        string `json:"priceRate"`
	AvgPrice         string `json:"avgPrice"`
	OrigType         string `json:"origType"`
	PositionSide     string `json:"positionSide"`
	PriceProtect     bool   `json:"priceProtect"`
	ClosePosition    bool   `json:"closePosition"`
	Market           int    `json:"market"`
}
