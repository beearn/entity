package entity

type MessageNotify struct {
	RecipientID int64
	Text        interface{}
	Format      string
}
