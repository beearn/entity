package entity

import "time"

type Crypto struct {
	Symbol     string
	Price      float64
	LastUpdate time.Time
}
