package entity

type Dashboard struct {
	Symbol string
	Period string
	Market int
	Klines []*Kline
}

type StochRSI struct {
	K []float64
	D []float64
}

type DashboardPeriods struct {
	Symbol  string
	Periods map[string]*Dashboard
}
